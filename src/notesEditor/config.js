import {
  BasePackage,
  EmphasisPackage,
  ParagraphPackage,
  ProseArticle,
  SpellCheckPackage
} from 'substance'

import CommentPackage from '../elements/comment/CommentPackage'
import IsolatedNote from '../elements/isolatedNote/IsolatedNotePackage'
import TrackChangePackage from '../elements/track_change/TrackChangePackage'

const config = {
  name: 'simple-editor',
  configure: (config, options) => {
    config.defineSchema({
      name: 'prose-article',
      ArticleClass: ProseArticle,
      defaultTextType: 'paragraph'
    })

    config.import(BasePackage, {
      noBaseStyles: options.noBaseStyles
    })
    config.import(ParagraphPackage)
    config.import(EmphasisPackage)
    config.import(SpellCheckPackage)
    config.import(CommentPackage)
    config.import(IsolatedNote)
    config.import(TrackChangePackage)
  }
}

export default config
