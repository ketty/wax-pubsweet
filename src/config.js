import {
  BasePackage,
  EmphasisPackage,
  HeadingPackage,
  LinkPackage,
  PersistencePackage,
  ProseArticle,
  SpellCheckPackage,
  SubscriptPackage,
  SuperscriptPackage
} from 'substance'

// My Elements
// import ChapterNumber from './elements/chapterNumber/ChapterNumberPackage'
import ChapterSubtitle from './elements/chapterSubtitle/ChapterSubtitlePackage'
// import ChapterTitle from './elements/chapterTitle/ChapterTitlePackage'
import CommentPackage from './elements/comment/CommentPackage'
import DiacriticsPackage from './elements/diacritics/DiacriticsPackage'
import EpigraphPoetry from './elements/epigraphPoetry/EpigraphPoetryPackage'
import EpigraphProse from './elements/epigraphProse/EpigraphProsePackage'
import ExtractPoetry from './elements/extractPoetry/ExtractPoetryPackage'
import ExtractProse from './elements/extractProse/ExtractProsePackage'
import ImagePackage from './elements/images/ImagePackage'
import LeftSwitchTextTypePackage from './elements/left_switch_text_type/LeftSwitchTextTypePackage'
import ListPackage from './elements/list/ListPackage'
import NotePackage from './elements/note/NotePackage'
import Paragraph from './elements/paragraph/ParagraphPackage'
import SourceNotePackage from './elements/source_note/SourceNotePackage'
import TrackChangePackage from './elements/track_change/TrackChangePackage'

const config = {
  name: 'simple-editor',
  configure: (config, options) => {
    config.defineSchema({
      name: 'prose-article',
      ArticleClass: ProseArticle,
      defaultTextType: 'paragraph'
    })

    config.import(BasePackage, {
      noBaseStyles: options.noBaseStyles
    })

    config.import(EmphasisPackage)
    config.addLabel('emphasis', 'Italics')
    config.import(SubscriptPackage)
    config.import(SuperscriptPackage)
    config.import(PersistencePackage)

    config.import(CommentPackage)
    config.import(ImagePackage)
    config.import(LeftSwitchTextTypePackage)
    config.import(LinkPackage)
    config.import(ListPackage)
    config.import(NotePackage)

    config.import(SpellCheckPackage)
    config.import(TrackChangePackage)

    config.import(Paragraph)
    // config.import(ChapterNumber)
    config.import(ChapterSubtitle)
    // config.import(ChapterTitle)
    config.import(ExtractProse)
    config.import(ExtractPoetry)
    config.import(EpigraphProse)
    config.import(EpigraphPoetry)
    config.import(SourceNotePackage)
    config.import(HeadingPackage)
    config.import(DiacriticsPackage)
  }
}

export default config
