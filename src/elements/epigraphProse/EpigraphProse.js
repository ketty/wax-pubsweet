import { TextBlock } from 'substance'

class EpigraphProse extends TextBlock {}

EpigraphProse.type = 'epigraph-prose'

export default EpigraphProse
