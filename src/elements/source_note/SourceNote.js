import { TextBlock } from 'substance'

class SourceNote extends TextBlock {}

SourceNote.type = 'source-note'

export default SourceNote
