import { Command } from 'substance'

class InsertListCommand extends Command {
  getCommandState (params) {
    let sel = this._getSelection(params)
    let commandState = {}
    let _disabledCollapsedCursor = this.config.disableCollapsedCursor && sel.isCollapsed()
    if (_disabledCollapsedCursor || !sel.isPropertySelection()) {
      commandState.disabled = true
    }
    return commandState
  }
  execute (params) {
    let ordered = this.config.ordered
    let customValue = null
    if (this.config.custom) {
      customValue = this.config.custom
    }

    let editorSession = params.editorSession
    editorSession.transaction((tx) => {
      if (customValue !== null) {
        tx.toggleList({ ordered: ordered, custom: customValue })
      } else {
        tx.toggleList({ ordered: ordered })
      }
    })
  }
}

export default InsertListCommand
