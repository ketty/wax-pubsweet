/* eslint react/prop-types: 0 */
import { Component } from 'substance'
import { each, groupBy, map } from 'lodash'

class DiacriticsList extends Component {
  static scrollToView (section) {
    const element = document.getElementById(section)
    element.scrollIntoView()
    element.className += ' active'

    setTimeout(() => {
      element.classList.remove('active')
    }, 1000)
  }

  render ($$) {
    const iconsList = groupBy(this.props.specialCharacters, 'group')

    const diacriticsList = $$('ul')
    .addClass('diacritics-list-container')

    const headerWrapper = $$('ul')
    .addClass('title-header-wrapper')

    each(iconsList, (list, title) => {
      const diacriticsHeader = $$('li')
      .addClass('diacritic-header-title')
      .append(title)
      .on('click', () => DiacriticsList.scrollToView(title))

      const diacriticsRow = $$('li')
      .addClass('diacritics-group')
      .attr('id', title)

      const listItems = map(list, (entry, name) => {
        const icon = $$('div')
          .addClass('diacritics-icon')
          .append(entry.unicode)
          .attr('title', entry.name)
          .on('click', () => this.props.onClick(entry.unicode))
        return icon
      })

      diacriticsRow.append(listItems)
      headerWrapper.append(diacriticsHeader)
      diacriticsList.append(diacriticsRow)
    })

    return $$('div')
    .addClass('menu-diacritics-wrapper')
    .append(headerWrapper)
    .append(diacriticsList)
  }
}

export default DiacriticsList
