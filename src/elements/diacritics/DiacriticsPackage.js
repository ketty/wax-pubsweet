import DiacriticsCommand from './DiacriticsCommand'
import DiacriticsControlTool from './DiacriticsControlTool'

export default {
  name: 'diacritics',
  configure: (config, { toolGroup }) => {
    config.addToolGroup('diacritics-tool')
    config.addTool('diacritics-tool', DiacriticsControlTool, {
      toolGroup: 'diacritics-tool'
    })
    config.addCommand('diacritics-tool', DiacriticsCommand)
    config.addIcon('diacritics-tool', { 'fontawesome': ' fa-ellipsis-h' })
    config.addLabel('diacritics-tool', {
      en: 'Diacritics Tool'
    })
  }
}
