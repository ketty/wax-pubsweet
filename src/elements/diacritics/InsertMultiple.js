/* eslint react/prop-types: 0 */
import { Component } from 'substance'

class InsertMultiple extends Component {
  render ($$) {
    const insertMultiple = $$('div')
    .addClass('insert-multiple')
    .append('Keep Open')
    .append($$('input')
    .attr('type', 'checkbox')
    .attr('name', 'multiple')
    .attr('value', '0')
    .attr('id', 'multiple-char')
    .on('click', this.props.insertMultipleChar)
    .ref('insert-multiple-chars')
  )

    return insertMultiple
  }
}

export default InsertMultiple
