import { FileProxy } from 'substance'

class ImageFileProxy extends FileProxy {

  constructor (fileNode, context) {
    super(fileNode, context)
    this.file = fileNode.sourceFile

    if (this.file) {
      this._fileUrl = URL.createObjectURL(this.file)
    }
    this.url = fileNode.url
  }

  getUrl () {
    // if we have fetched the url already, just serve it here
    if (this.url) {
      return this.url
    }
    // if we have a local file, use it's data URL
    if (this._fileUrl) {
      return this._fileUrl
    }
    // no URL available
    return ''
  }

  sync () {
    if (!this.url) {
      this.context.editorSession.saveHandler.uploadFile(this.file).then((res) => {
        this.url = res.file
        FileProxy.prototype.triggerUpdate.call(this)
      })
    }
  }
}

ImageFileProxy.match = function(fileNode, context) { // eslint-disable-line
  return fileNode.fileType === 'image'
}

export default ImageFileProxy
