import { BlockNodeComponent, deleteNode } from 'substance'

class ImageComponent extends BlockNodeComponent {

  didMount () {
    super.didMount.call(this)
    this.context.editorSession.onRender('document', this._onDocumentChange, this)
  }

  dispose () {
    super.dispose.call(this)
    this.context.editorSession.off(this)
  }

  _onDocumentChange (change) {
    if (change.isAffected(this.props.node.id) ||
      change.isAffected(this.props.node.imageFile)) {
      this.rerender()
    }
  }

  render ($$) {
    let el = super.render($$)
    el.addClass('sc-image')
    el.append(
      $$('img').attr({
        src: this.props.node.getUrl()
      }).ref('image')
    )

    const editor = this.getEditor()
    editor.emit('ui:updated')

    return el
  }

  getEditor () {
    return this.context.editor
  }
}

export default ImageComponent
