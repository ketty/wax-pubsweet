import { each } from 'lodash'
import { Modal } from 'substance'

class ModalWarning extends Modal {
  constructor (props) {
    super(props)

    this.parent.on('send:route', this.getNextRoute, this)
    this.route = ''
    this.action = 'PUSH'
    this.save = false
  }

  render ($$) {
    const el = $$('div')
        .addClass('sc-modal')

    const modalHeader = $$('div')
         .addClass('sc-modal-header')
         .append('There are unsaved changes. Do you want to save your work?')

    const saveExit = $$('button')
      .addClass('sc-modal-button')
      .addClass('sc-modal-button-save-exit')
      .append('Save and quit')
      .on('click', this.saveExitWriter)

    const exit = $$('button')
      .addClass('sc-modal-button')
      .addClass('sc-modal-button-exit')
      .append('Quit without saving')
      .on('click', this.exitWriter)

    const cancelMessage = $$('span')
      .append('Take me back to the editor')

    const cancel = $$('button')
      .addClass('sc-modal-button')
      .addClass('sc-modal-button-close')
      .append(cancelMessage)
      .on('click', this.closeModal)

    const modalActionsContainer = $$('div')
          .addClass('sc-modal-actions')
          .append(
            saveExit,
            exit,
            cancel
          )

    if (this.props.width) {
      el.addClass(`sm-width-${this.props.width}`)
    }

    el.append(
      $$('div').addClass('se-body')
      .append(modalHeader)
      .append(modalActionsContainer)
    )

    return el
  }

  getNextRoute (args) {
    this.route = args.location
    this.action = args.action
  }

  closeModal () {
    this.send('closeModal')
  }

  editorSessionSave () {
    if (!this.save) {
      each(this.context.editorSession._history.doneChanges, (key) => {
        this.context.editorSession.undo()
      })
    }

    return new Promise((resolve, reject) => {
      resolve(this.context.editorSession.save())
    })
  }

  saveExitWriter () {
    this.save = true
    this.goToLocation()
  }

  exitWriter () {
    this.goToLocation()
  }

  goToLocation () {
    this.editorSessionSave().then(() => {
      if (this.action === 'POP') {
        this.context.editor.props.history.go(-1)
      } else {
        this.context.editor.props.history.push(this.route)
      }
    })
  }
}

export default ModalWarning
