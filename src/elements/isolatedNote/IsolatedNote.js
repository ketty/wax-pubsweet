import { DocumentNode } from 'substance'

class IsolatedNote extends DocumentNode {}

IsolatedNote.define({
  type: 'isolated-note',
  content: 'text',
  calloutId: 'string',
  position: 'string'
})

export default IsolatedNote
