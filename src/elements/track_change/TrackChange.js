import { PropertyAnnotation } from 'substance'

class TrackChange extends PropertyAnnotation {}

TrackChange.define({
  status: { type: 'string' },
  user: {
    id: 'string',
    roles: 'array',
    username: 'string'
  }
})

TrackChange.type = 'track-change'

export default TrackChange
