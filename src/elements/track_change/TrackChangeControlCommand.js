import { Command } from 'substance'

class TrackChangeControlCommand extends Command {
  getCommandState (params) {
    const newState = {
      disabled: false,
      active: false
    }

    return newState
  }

  execute (params, context) {
    const surface = context.surfaceManager.getSurface('body')
    surface.send('trackChangesUpdate')

    // put the cursor back where it was on the surface
    if (!params.selection.isNull()) {
      params.editorSession.setSelection(params.selection)
    }

    return true
  }
}

TrackChangeControlCommand.type = 'track-change-enable'

export default TrackChangeControlCommand
