import TrackChange from './TrackChange'
import TrackChangeCommand from './TrackChangeCommand'
import TrackChangeComponent from './TrackChangeComponent'
import TrackChangeHTMLConverter from './TrackChangeHTMLConverter'
import TrackChangeControlTool from './TrackChangeControlTool'
import TrackChangeControlViewTool from './TrackChangeControlViewTool'
import TrackChangeControlCommand from './TrackChangeControlCommand'
import TrackChangeControlViewCommand from './TrackChangeControlViewCommand'

export default {
  name: 'track-change',
  configure: function (config, { toolGroup }) {
    config.addNode(TrackChange)
    config.addToolGroup('track-change-enable')
    config.addToolGroup('track-change-toggle-view')
    config.addComponent(TrackChange.type, TrackChangeComponent)
    config.addConverter('html', TrackChangeHTMLConverter)

    // TODO -- both tools should go to the same target
    config.addTool('track-change-enable', TrackChangeControlTool, {
      toolGroup: 'track-change-enable'
    })

    config.addTool('track-change-toggle-view', TrackChangeControlViewTool, {
      toolGroup: 'track-change-toggle-view'
    })

    config.addCommand('track-change-enable', TrackChangeControlCommand)
    config.addCommand('track-change-toggle-view', TrackChangeControlViewCommand)
    config.addCommand(TrackChange.type, TrackChangeCommand, { nodeType: TrackChange.type })

    config.addIcon('track-change', { 'fontawesome': 'fa-eye' })

    config.addLabel('track-change-enable', {
      en: 'Toggle recording changes'
    })
    config.addLabel('track-change-toggle-view', {
      en: 'Toggle tracked changes view'
    })
  }
}
