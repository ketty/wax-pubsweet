import { AnnotationComponent } from 'substance'

class TrackChangeComponent extends AnnotationComponent {

  didMount () {
    const { editorSession } = this.context
    editorSession.onUpdate('document', this.onTrackChangesUpdated, this)
  }

  render ($$) {
    const { id, status, user } = this.props.node
    const canAct = this.canAct()
    const viewMode = this.getViewMode()

    const accept = $$('a')
      .addClass('sc-track-item-accept')
      .on('click', this.accept)

    const reject = $$('a')
      .addClass('sc-track-item-reject')
      .on('click', this.reject)

    const container = $$('span')
      .addClass('sc-accept-reject-container')
      .append(accept)
      .append(reject)

    if (this.shouldHideContainer()) {
      container.addClass('sc-accept-reject-container-hide')
    }

    let el = $$('span')
      .attr('data-id', id)
      .attr('title', user.username)
      .addClass(this.getClassNames())
      .append(this.props.children)

    if (canAct) el.append(container)

    el.addClass('sc-track-change-' + status)

    if (viewMode === false) {
      if (status === 'delete') el.addClass('sc-track-delete-hide')
      if (status === 'add') el.addClass('sc-track-add-show')
    }

    const statusClass = 'sc-track-change-' + status
    el.addClass(statusClass)

    return el
  }

  canAct () {
    const provider = this.getProvider()
    return provider.canAct()
  }

  accept () {
    this.resolve('accept')
  }

  reject () {
    this.resolve('reject')
  }

  resolve (action) {
    const annotation = this.props.node
    const provider = this.getProvider()

    provider.resolve(annotation, action)
  }

  getEditor () {
    return this.context.editor
  }

  getProvider () {
    return this.context.trackChangesProvider
  }

  getViewMode () {
    const editor = this.getEditor()
    const { trackChangesView } = editor.state
    return trackChangesView
  }

  shouldHideContainer () {
    const annotation = this.props.node
    const annotationSelection = annotation.getSelection()

    const surface = this.context.surface

    const selection = surface.editorSession.getSelection()

    if (selection.isNodeSelection || selection.isNull() || selection.isCollapsed()) return false

    const overlaps = selection.overlaps(annotationSelection)
    const contains = selection.contains(annotationSelection)

    if (overlaps && !contains) return true
    return false
  }

  onTrackChangesUpdated (change) {
    const trackChangesProvider = this.getProvider()
    trackChangesProvider.handleDocumentChange(change)
  }
}

export default TrackChangeComponent
