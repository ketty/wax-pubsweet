import { clone, each, filter, maxBy, minBy } from 'lodash'

import {
  getAllAnnotationsByStatus,
  isAnnotationFromTheSameUser
} from './annotationHelpers'

import {
  getSelection,
  moveCursorTo,
  setSelectionPlusOne
} from './selectionHelpers'

import {
  createTrackAnnotation,
  deleteSelection,
  insertText,
  expandTrackAnnotation,
  removeTrackAnnotation,
  truncateTrackAnnotation
} from './transformations'

const createAdditionAnnotationOnLastChar = options => {
  const { surface } = options
  options.selection = getSelection(surface)

  setSelectionPlusOne(options, 'left')
  createTrackAnnotation(options)
  moveCursorTo(options, 'end')
}

const deleteAllOwnAdditions = options => {
  const { selection, surface } = options
  const originalSelection = selection
  // const originalSelection = selection || this.getSelection()
  let shortenBy = 0

  const additions = getAllAnnotationsByStatus(options, 'add')

  const ownAdditions = filter(additions, annotation => {
    return isAnnotationFromTheSameUser(options, annotation)
  })

  each(ownAdditions, (annotation) => {
    const selection = annotation.getSelection()

    // make sure only the part of the annotation that is selected is deleted
    if (annotation.start.offset < originalSelection.start.offset) {
      selection.start.offset = originalSelection.start.offset
    }

    if (annotation.end.offset > originalSelection.end.offset) {
      selection.end.offset = originalSelection.end.offset
    }

    shortenBy += (selection.end.offset - selection.start.offset)
    // console.log(shortenBy)

    const options = { selection, surface }
    deleteSelection(options)
  })

  // throw klfjdskljfsj

  return shortenBy    // return how much shorter the selection should now be
}

const deleteOrMergeAllOwnDeletions = (options, selection) => {
  const { surface } = options

  const deletions = clone(getAllAnnotationsByStatus(options, 'delete'))
  const ownDeletions = filter(deletions, annotation => {
    return isAnnotationFromTheSameUser(options, annotation)
  })

  const selectionArray = [selection]

  each(ownDeletions, (annotation) => {
    const annotationSelection = annotation.getSelection()
    const contained = selection.contains(annotationSelection)

    if (!contained) {
      selectionArray.push(annotationSelection)
    }

    removeTrackAnnotation({ annotation, surface })
  })

  selection.start.offset = minBy(selectionArray, 'start.offset').start.offset
  selection.end.offset = maxBy(selectionArray, 'end.offset').end.offset

  return selection
  // TODO
  // this.updateSelection(selection, startOffset, endOffset)
}

const deleteSelectedAndCreateAddition = (options) => {
  options.status = 'delete'
  createTrackAnnotation(options)
  moveCursorTo(options, 'end')

  // selection is now collapsed, so handle it as collapsed
  options.status = 'add'
  options.selection = getSelection(options.surface)

  const provider = getProvider(options)
  provider.handleAddCollapsed(options)
}

const getProvider = (options) => {
  const { surface } = options
  return surface.context.trackChangesProvider
}

const expandAnnotationToDirection = (options, annotation) => {
  const { surface } = options
  const move = options.move || 'left'
  const cursorTo = options.cursorTo || 'end'

  options.annotation = annotation
  // const selection = setSelectionPlusOne(options, move)
  options.selection = getSelection(surface)
  setSelectionPlusOne(options, move)

  expandTrackAnnotation(options)
  moveCursorTo(options, cursorTo)
}

const insertCharacterWithAddAnnotation = (options) => {
  insertText(options)

  // TODO -- watch it with additions by other users
  createAdditionAnnotationOnLastChar(options)
}

const insertCharacterWithoutExpandingAnnotation = (options, annotation) => {
  // After text has been inserted, set the event to null to make sure it does
  // not get added twice by a second iteration of the handlers.
  // eg. if at the end of the logic path, handleAddCollapsed gets called
  //     that will assume that there is text still to be inserted
  insertText(options)
  options.event = null

  // Clone the options, to make sure that the annotation or the selection
  // does not change while the truncate operation is in progress.
  const opts = clone(options)

  opts.selection = getSelection(opts.surface)
  opts.selection = setSelectionPlusOne(opts, 'left')
  opts.annotation = annotation
  opts.doc = opts.editorSession.getDocument()  // TODO -- should get from handler

  truncateTrackAnnotation(opts)
  moveCursorTo(opts, 'end')
}

const markSelectionAsDeleted = (options) => {
  // const { direction, selection } = options
  const { direction } = options
  options.status = 'delete'
  createTrackAnnotation(options)
  moveCursorTo(options, direction.cursorTo)
}

const selectCharacterAndMarkDeleted = (options) => {
  const { direction, surface } = options

  setSelectionPlusOne(options, direction.move)
  const selection = getSelection(surface)
  options.selection = selection

  options.status = 'delete'
  createTrackAnnotation(options)
  moveCursorTo(options, direction.cursorTo)
}

export {
  createAdditionAnnotationOnLastChar,
  deleteAllOwnAdditions,
  deleteOrMergeAllOwnDeletions,
  deleteSelectedAndCreateAddition,
  expandAnnotationToDirection,
  insertCharacterWithAddAnnotation,
  insertCharacterWithoutExpandingAnnotation,
  markSelectionAsDeleted,
  selectCharacterAndMarkDeleted
}
