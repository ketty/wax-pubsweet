import { filter, find } from 'lodash'

import { getSelection } from './selectionHelpers'

const getAllAnnotationsByStatus = (options, status) => {
  const annotations = getAllExistingTrackAnnotations(options)

  const annotationsByStatus = filter(annotations, (annotation) => {
    return annotation.status === status
  })

  return annotationsByStatus
}

const getAllExistingTrackAnnotations = (options) => {
  // const editorSession = this.getEditorSession()
  const { editorSession } = options

  const selectionState = editorSession.getSelectionState()
  const annotations = selectionState.getAnnotationsForType('track-change')

  return annotations
}

// TODO -- handle multiple delete and add annotations
const getAnnotationByStatus = (options, status) => {
  const annotationsForStatus = getAllAnnotationsByStatus(options, status)
  return annotationsForStatus[0]
}

const getAnnotationUser = (annotation) => {
  return annotation.user.id
}

const isAnnotationFromTheSameUser = (options, annotation) => {
  const annotationUser = getAnnotationUser(annotation)
  const currentUser = options.user.id

  if (annotationUser === currentUser) return true
  return false
}

const isNotOnTrackAnnotation = (options) => {
  const annotations = getAllExistingTrackAnnotations(options)
  return (annotations.length === 0)
}

// returns whether the selection is on an add / delete tracked change
const isOnAnnotation = (options, status) => {
  const annotations = getAllExistingTrackAnnotations(options)

  const annotation = find(annotations, (annotation) => {
    return annotation.status === status
  })

  if (!annotation) return false
  return true
}

const isSelectionOnLeftEdge = (options, annotation) => {
  // const selection = this.getSelection()
  const selection = getSelection(options.surface)
  return (selection.start.offset === annotation.start.offset)
}

const isSelectionOnRightEdge = (options, annotation) => {
  // const selection = this.getSelection()
  const selection = getSelection(options.surface)
  return (selection.end.offset === annotation.end.offset)
}

export {
  getAllAnnotationsByStatus,
  getAnnotationByStatus,
  isAnnotationFromTheSameUser,
  isNotOnTrackAnnotation,
  isOnAnnotation,
  isSelectionOnLeftEdge,
  isSelectionOnRightEdge
}
