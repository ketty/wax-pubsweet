import { annotationHelpers } from 'substance'

/*

  TEXT TRANSFORMATIONS

*/

const deleteCharacter = options => {
  // const surface = this.getSurface()
  const { direction, surface } = options
  const move = direction.move
  const info = { action: 'delete' }

  const transformation = (tx, args) => {
    args.direction = move
    return tx.deleteCharacter(move)
  }

  surface.editorSession.transaction(transformation, info)
}

const deleteSelection = options => {
  const { selection, surface } = options
  const containerId = surface.containerId
  const info = { action: 'delete' }

  const transformation = (tx, args) => {
    tx.setSelection({
      type: 'property',
      path: selection.path,
      surfaceId: containerId,
      startOffset: selection.start.offset,
      endOffset: selection.end.offset
    })

    return tx.deleteSelection()
  }

  surface.editorSession.transaction(transformation, info)
}

const insertText = options => {
  const { event, surface } = options
  if (!event) return

  surface.editorSession.transaction(function (tx, args) {
    if (surface.domSelection) surface.domSelection.clear()
    args.text = event.data || ' '    // if no data, it's a space key
    return tx.insertText(args.text)
  }, { action: 'type' })
}

/*

  ANNOTATION TRANSFORMATIONS

*/

const createTrackAnnotation = options => {
  const { selection, status, surface, user } = options
  const info = getInfo()

  // console.log(selection)

  // TODO -- these two ifs don't work anymore
  if (selection.isContainerSelection()) {
    return console.warn('Cannot delete a container')
  }

  if (selection.isNodeSelection()) {
    // console.log('node selection!')
    if (selection.isCollapsed()) {
      // options.direction.
      return this.deleteCharacter('left')
    }
    return this.deleteSelection(selection)
  }

  const transformation = (tx, args) => {
    const newNode = {
      selection: selection,
      status: status,
      type: 'track-change',
      path: selection.path,
      start: selection.start,
      end: selection.end,
      user: {
        id: user.id,
        roles: user.roles,
        username: user.username
      }
    }
    tx.create(newNode)
  }

  surface.editorSession.transaction(transformation, info)
}

const expandTrackAnnotation = options => {
  const { annotation, selection, surface } = options
  const info = getInfo()

  const transformation = (tx, args) => {
    args.selection = selection
    args.anno = annotation

    annotationHelpers.expandAnnotation(tx, args.anno, args.selection)
  }

  surface.editorSession.transaction(transformation, info)
}

const removeTrackAnnotation = options => {
  const { annotation, surface } = options

  const transformation = (tx, args) => {
    tx.delete(annotation.id)
  }

  surface.editorSession.transaction(transformation)
}

const truncateTrackAnnotation = options => {
  const { annotation, doc, selection, surface } = options
  const info = getInfo()

  const transformation = (tx, args) => {
    // console.log(selection)
    annotationHelpers.truncateAnnotation(doc, annotation, selection)
  }

  surface.editorSession.transaction(transformation, info)
}

// Prevent substance from running getBoundingRectangle,
// as we will unset the selection manually.

const getInfo = () => {
  return { skipSelection: true }
}

/*

  EXPORT

*/

export {
  createTrackAnnotation,
  deleteCharacter,
  deleteSelection,
  expandTrackAnnotation,
  insertText,
  removeTrackAnnotation,
  truncateTrackAnnotation
}
