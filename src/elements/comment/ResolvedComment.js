import { PropertyAnnotation } from 'substance'

class ResolvedComment extends PropertyAnnotation {}

ResolvedComment.type = 'resolved-comment'

export default ResolvedComment
