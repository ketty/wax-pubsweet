import ExtractProse from './ExtractProse'
import ExtractProseComponent from './ExtractProseComponent'
import ExtractProseHTMLConverter from './ExtractProseHTMLConverter'

export default {
  name: 'extract-prose',
  configure: (config) => {
    config.addNode(ExtractProse)

    config.addComponent(ExtractProse.type, ExtractProseComponent)
    config.addConverter('html', ExtractProseHTMLConverter)

    config.addTextType({
      name: 'extract-prose',
      data: { type: 'extract-prose' }
    })

    config.addLabel('extract-prose', {
      en: 'Extract: Prose'
    })
  }
}
