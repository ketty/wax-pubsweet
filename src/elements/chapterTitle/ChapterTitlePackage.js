import ChapterTitle from './ChapterTitle'
import ChapterTitleComponent from './ChapterTitleComponent'
import ChapterTitleHTMLConverter from './ChapterTitleHTMLConverter'

export default {
  name: 'chapter-title',
  configure: (config) => {
    config.addNode(ChapterTitle)

    config.addComponent(ChapterTitle.type, ChapterTitleComponent)
    config.addConverter('html', ChapterTitleHTMLConverter)

    config.addTextType({
      name: 'chapter-title',
      data: { type: 'chapter-title' }
    })

    config.addLabel('chapter-title', {
      en: 'Chapter Title'
    })
  }
}
