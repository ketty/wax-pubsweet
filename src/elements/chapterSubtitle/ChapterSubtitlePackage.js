import ChapterSubtitle from './ChapterSubtitle'
import ChapterSubtitleComponent from './ChapterSubtitleComponent'
import ChapterSubtitleHTMLConverter from './ChapterSubtitleHTMLConverter'

export default {
  name: 'chapter-subtitle',
  configure: (config) => {
    config.addNode(ChapterSubtitle)

    config.addComponent(ChapterSubtitle.type, ChapterSubtitleComponent)
    config.addConverter('html', ChapterSubtitleHTMLConverter)

    config.addTextType({
      name: 'chapter-subtitle',
      data: { type: 'chapter-subtitle' }
    })

    config.addLabel('chapter-subtitle', {
      en: 'Subtitle'
    })
  }
}
