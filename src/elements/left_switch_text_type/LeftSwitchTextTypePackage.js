import { SwitchTextTypeCommand } from 'substance'
import LeftSwitchTextTypeTool from './LeftSwitchTextTypeTool'

// TODO
// this package should have a better name

export default {
  name: 'switch-text-type',
  configure: (config, options) => {
    config.addToolGroup('text')
    config.addCommand('switch-text-type', SwitchTextTypeCommand)

    // is this || here necessary?
    config.addTool('switch-text-type', LeftSwitchTextTypeTool, {
      toolGroup: options.toolGroup || 'text'
    })
  }
}
