import Paragraph from './Paragraph'
import ParagraphComponent from './ParagraphComponent'
import ParagraphHTMLConverter from './ParagraphHTMLConverter'

export default {
  name: 'paragraph',
  configure: (config) => {
    config.addNode(Paragraph)

    config.addComponent(Paragraph.type, ParagraphComponent)
    config.addConverter('html', ParagraphHTMLConverter)

    config.addTextType({
      name: 'paragraph',
      data: { type: 'paragraph' }
    })

    config.addLabel('paragraph', {
      en: 'General Text'
    })
  }
}
