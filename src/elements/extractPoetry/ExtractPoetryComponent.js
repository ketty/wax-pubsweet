import { TextBlockComponent } from 'substance'

class ExtractPoetryComponent extends TextBlockComponent {
  render ($$) {
    const el = super.render($$)
    return el.addClass('sc-blockquote')
  }
}

export default ExtractPoetryComponent
