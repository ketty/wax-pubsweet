import ExtractPoetry from './ExtractPoetry'
import ExtractPoetryComponent from './ExtractPoetryComponent'
import ExtractPoetryHTMLConverter from './ExtractPoetryHTMLConverter'

export default {
  name: 'extract-poetry',
  configure: (config) => {
    config.addNode(ExtractPoetry)

    config.addComponent(ExtractPoetry.type, ExtractPoetryComponent)
    config.addConverter('html', ExtractPoetryHTMLConverter)

    config.addTextType({
      name: 'extract-poetry',
      data: { type: 'extract-poetry' }
    })

    config.addLabel('extract-poetry', {
      en: 'Extract: Poetry'
    })
  }
}
