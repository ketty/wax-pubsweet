import _ from 'lodash'
import { TOCProvider as TocProvider } from 'substance'

class CommentsProvider extends TocProvider {
  // TODO -- works if I rename to doc
  constructor (document, props) {
    super(document, props)
    this.activeEntry = null
    const editorSession = props.editorSession

    editorSession.onUpdate('', this.updateActiveEntry, this)
    editorSession.onRender('document', this.update, this)

    const editor = props.controller
    editor.on('ui:updated', this.onUiUpdate, this)

    // TODO is this needed anymore?
    // const doc = editorSession.getDocument()
    // doc.on('document:changed', this.update, this)
  }

  //
  // entries

  computeEntries () {
    const comments = this.getComments()
    const entries = this.sortNodes(comments)
    return entries
  }

  update () {
    this.emit('comments:updated')
  }

  // TODO -- this will probably not be needed if we stop moving the
  // line height for track changes
  // offset by the time it takes the animation to change the line height
  onUiUpdate () {
    setTimeout(() => { this.update() }, 100)
  }

  reComputeEntries () {
    this.entries = this.computeEntries()
    this.update()
  }

  //
  // active entry

  getActiveEntry () {
    return this.activeEntry
  }

  setActiveEntry (id) {
    this.removeActivePropFromNode(this.activeEntry)
    this.addActivePropToNode(id)

    this.activeEntry = id
    this.update()
  }

  removeActiveEntry () {
    this.removeActivePropFromNode(this.activeEntry)

    this.activeEntry = null
    this.update()
  }

  // runs when document session is updated
  updateActiveEntry () {
    const activeEntry = this.getActiveEntry()
    const selectionContainsComments = this.selectionContainsComments()

    // neither current or previous selection is a comment, ignore
    if (!selectionContainsComments && !activeEntry) return

    // previous selection was a comment, but new one isn't
    // or the selection contains a comment, but is larger than it,
    // allowing you to make an overlapping comment
    // in both cases, remove the active comment
    const isSelectionLargerThanComments = this.isSelectionLargerThanComments()
    if (
      !selectionContainsComments && activeEntry ||
      isSelectionLargerThanComments
    ) {
      this.removeEmptyComments()
      this.removeActiveEntry()
      return
    }

    const activeComment = this.getActiveComment()
    if (!activeComment) return this.removeActiveEntry()

    // current selection and previous selection both belong to the same comment
    if (activeEntry === activeComment.id) return

    // new active comment
    this.setActiveEntry(activeComment.id)
  }

  //
  // resolved comments

  resolveComment (id) {
    const self = this

    const ds = this.config.editorSession
    const doc = ds.getDocument()
    const commentNode = doc.get(id)

    const path = commentNode.path
    const startOffset = commentNode.start.offset
    const endOffset = commentNode.end.offset

    const sel = ds.createSelection(path, startOffset, endOffset)

    const resolvedNodeData = {
      selection: sel,
      type: 'resolved-comment',
      path: sel.path,
      start: sel.start,
      end: sel.end
    }

    // create resolved comment annotation on the same selection the
    // comment was on and remove existing comment
    ds.transaction(function (tx, args) {
      const annotation = tx.create(resolvedNodeData)
      const resolvedCommentId = annotation.id
      self.markCommentAsResolved(id, resolvedCommentId)
    })

    this.deleteCommentNode(id)
    this.reComputeEntries()
  }

  //
  // focus text area for a comment reply

  focusTextArea (id) {
    setTimeout(function () {
      var textarea = document.getElementById(id)
      if (textarea) {
        textarea.focus()
      }
    }, 10)
  }

  //
  // remove comments from pane if the user wrote nothing in them

  removeEmptyComments () {
    const self = this

    const comments = self.getComments()
    const commentsContent = self.config.comments

    _.each(comments, function (value, key) {
      if (commentsContent && !commentsContent[key]) {
        self.deleteCommentNode(key)
      }
    })
  }

  //
  //
  // helpers
  //

  deleteCommentNode (id) {
    const surface = this.getSurface()

    surface.editorSession.transaction(function (tx, args) {
      tx.delete(id)
      return args
    })
    surface.rerender()
  }

  getActiveComment () {
    const comments = this.getSelectionComments()
    const mode = this.getMode()
    const selection = this.getSelection()

    // we have a non-collapsed selection that contains multiple comments
    if (mode === 'fuse') {
      // only keep the comments that contain the whole selection
      let annos = _.filter(comments, annotation => {
        return annotation.getSelection().contains(selection)
      })

      if (annos.length === 0) return null
      if (annos.length === 1) return annos[0]

      // find comments that contain all other comments and remove them
      // the focus of the active comment should be inwards
      let remove = []
      _.forEach(annos, anno => {
        let removeIt = true

        // TODO -- there's gotta be a better way to do this than a double loop
        _.forEach(annos, innerAnno => {
          if (anno === innerAnno) return

          const outerSelection = anno.getSelection()
          const innerSelection = innerAnno.getSelection()
          if (!outerSelection.contains(innerSelection)) removeIt = false
        })

        if (removeIt) remove.push(anno)
      })

      annos = _.difference(annos, remove)

      // once you have all comments that do not contain each other wholly
      // just choose the one on the left
      return _.minBy(annos, 'start.offset')
    }

    return comments[0]
  }

  // returns an array of all comments in selection
  getSelectionComments () {
    const selectionState = this.config.editorSession.getSelectionState()
    return selectionState.getAnnotationsForType('comment')
  }

  getComments () {
    const doc = this.getDocument()
    const nodes = doc.getNodes()
    // get all notes from the document
    const comments = _.pickBy(nodes, function (value, key) {
      return value.type === 'comment'
    })
    return comments
  }

  getCommentState () {
    const { commandManager } = this.config
    const commandStates = commandManager.getCommandStates()
    return commandStates.comment
  }

  getMode () {
    const commentState = this.getCommentState()
    return commentState.mode
  }

  getNode (id) {
    const doc = this.getDocument()
    return doc.get(id)
  }

  getEditorSession () {
    return this.config.editorSession
  }

  // returns the combined borders of all comment annotations in selection
  getCommentBorders () {
    const comments = this.getSelectionComments()
    if (comments.length === 0) return

    const minComment = _.minBy(comments, 'start.offset')
    const maxComment = _.maxBy(comments, 'end.offset')

    const min = minComment.start.offset
    const max = maxComment.end.offset

    return {
      start: min,
      end: max
    }
  }

  getSelection () {
    const editorSession = this.getEditorSession()
    return editorSession.getSelection()
  }

  getSurface () {
    const { containerId, surfaceManager } = this.config
    return surfaceManager.getSurface(containerId)
  }

  isSelectionLargerThanComments () {
    const commentBorders = this.getCommentBorders()
    if (!commentBorders) return false

    const selection = this.getSelection()

    if (
      selection.start.offset < commentBorders.start ||
      selection.end.offset > commentBorders.end
    ) return true

    return false
  }

  selectionContainsComments () {
    const commentAnnotations = this.getSelectionComments()
    return commentAnnotations.length > 0
  }

  addActivePropToNode (id) {
    this.updateActivePropForNode(id, true)
  }

  removeActivePropFromNode (id) {
    this.updateActivePropForNode(id, false)
  }

  updateActivePropForNode (id, bool) {
    if (!id) return
    const node = this.getNode(id)
    if (!node) return
    node.active = bool
  }

  // // TODO -- do I need to override this?
  // handleDocumentChange (change) {
  //   var doc = this.getDocument()
  //   var needsUpdate = false
  //   var tocTypes = this.constructor.tocTypes
  //
  //   for (var i = 0; i < change.ops.length; i++) {
  //     var op = change.ops[i]
  //     var nodeType
  //     if (op.isCreate() || op.isDelete()) {
  //       var nodeData = op.getValue()
  //       nodeType = nodeData.type
  //       if (_.includes(tocTypes, nodeType)) {
  //         needsUpdate = true
  //         break
  //       }
  //     } else {
  //       var id = op.path[0]
  //       var node = doc.get(id)
  //       if (node && _.includes(tocTypes, node.type)) {
  //         needsUpdate = true
  //         break
  //       }
  //     }
  //   }
  //
  //   if (needsUpdate) {
  //     // need a timeout here, to make sure that the updated doc has rendered
  //     // the annotations, so that the comment box list can be aligned with them
  //     const self = this
  //     setTimeout(function () {
  //       self.reComputeEntries()
  //     })
  //   }
  // }

  /*
  When a comment gets resolved the comment on the fragment gets a resolved property.
  The resolved property is the id of the new resolved comment annotation.
  If the user does not save the document after resolving, the id for the comment is still there, so it will display correctly.
  If the user resolves the comment, does not save and then resolves the same comment again, the resolve property will simply be overwritten.
  If the document is saved, the comment id will not exist in the document any more, so it will not be rendered.
  If the now resolved comment needs to be unresolved, the original contents can easily be found by looking for the object with the resolved comment's id in the resolved property.
  */
  markCommentAsResolved (commentId, resolvedId) {
    // const comments = this.config.comments
    // const update = this.config.update
    const { comments, fragment, update } = this.config
    const id = commentId

    if (!comments[id]) return
    comments[id].resolved = resolvedId

    fragment.comments = comments
    update(fragment)
  }

  sortNodes (nodes) {
    let comments = _.clone(nodes)
    const doc = this.getDocument()
    const { containerId } = this.config
    const container = doc.get(containerId)

    // sort notes by
    //   the index of the containing block
    //   their position within that block
    comments = _.map(comments, function (comment) {
      const blockId = comment.path[0]
      let blockPosition = container.getPosition(blockId)

      const nodePosition = comment.start.offset

      return {
        id: comment.id,
        blockPosition: blockPosition,
        nodePosition: nodePosition,
        node: comment
      }
    })

    return _.sortBy(comments, ['blockPosition', 'nodePosition'])
  }
}

CommentsProvider.tocTypes = ['comment']

export default CommentsProvider
