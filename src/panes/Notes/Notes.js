/* eslint react/prop-types: 0 */

import {
  Component,
  EditorSession,
  ProseEditorConfigurator as Configurator
} from 'substance'

import NotesEditor from '../../notesEditor/NotesEditor'
import config from '../../notesEditor/config'
import Importer from '../../notesEditor/NotesEditorImporter'
import SimpleExporter from '../../SimpleEditorExporter'

class Notes extends Component {
  constructor (props) {
    super(props)

    // TODO -- do we need the binds?
    this.resize = this.resize.bind(this)
    this.stopResize = this.stopResize.bind(this)

    // TODO -- we need sth better than an empty string
    this.notesPaneHeight = ''
  }

  didMount () {
    const notesProvider = this.getProvider()
    notesProvider.on('notes:updated', this.rerender, this)
  }

  didUpdate () {
    // TODO -- could be cleaner?
    this.el.el.style.height = this.notesPaneHeight + 'px'
    this.computeMainPane()
  }

  render ($$) {
    const {
      book,
      comments,
      containerId,
      disabled,
      fragment,
      history,
      trackChanges,
      trackChangesView,
      update,
      user
    } = this.props

    const { editorSession, configurator } = this.initNotesEditor()

    // remove dragmanager, as it gets attached to the html document el
    // the main editor also gets a dragmanager attached to the document
    // and causes problems when trying to dispose of them
    editorSession.dragManager.dispose()

    const title = $$('span').append('Notes')

    const titleContainer = $$('div')
      .addClass('notes-title')
      .append(title)

    const resizer = $$('div').addClass('resize-area')

    const el = $$('div')
         .addClass('notes-container')
         .append(titleContainer)
         .append(resizer)

    // TODO -- if not fragment, why return anything? should come first
    if (!fragment) return el

    const notesEditor = $$(NotesEditor, {
      book,
      editorSession,
      comments,
      configurator,
      containerId,
      history,
      disabled,
      fragment,
      trackChanges,
      trackChangesView,
      update,
      user
    })

    el.append(notesEditor)

    // TODO -- needs to be in render?
    resizer.addEventListener('mousedown', this.initResize, false)

    return el
  }

  // TODO -- avoid function names with underscore
  initNotesEditor () {
    const configurator = new Configurator().import(config)
    configurator.addImporter('html', Importer)
    const importer = configurator.createImporter('html')

    const provider = this.getProvider()

    const notes = provider.computeEntries()
    const exporter = new SimpleExporter(configurator.config)
    let noteContent = ''

    for (let i = 0; i < notes.length; i++) {
      const isolatedNoteElement = exporter.createElement('isolated-note')
      isolatedNoteElement.setAttribute('data-parent-id', notes[i].id)
      isolatedNoteElement.setAttribute('data-position', i + 1)
      isolatedNoteElement.innerHTML = notes[i]['note-content']
      noteContent += isolatedNoteElement.outerHTML
    }

    const doc = importer.importDocument(noteContent)

    const editorSession = new EditorSession(doc, {
      configurator
    })

    return {
      editorSession,
      configurator
    }
  }

  // TODO -- if we don't use the event, it shouldn't be in the args
  initResize (e) {
    window.addEventListener('mousemove', this.resize, false)
    window.addEventListener('mouseup', this.stopResize, false)
  }

  // TODO -- should be cleaner
  resize (e) {
    const { containerId } = this.props
    const elementOffset = this.el.el.getBoundingClientRect().top
    this.notesPaneHeight = (this.el.el.offsetHeight + elementOffset) - e.clientY
    const scrollPane = document.getElementById('content-panel-' + containerId).children
    scrollPane[0].style.minHeight = this.notesPaneHeight - 40 + 'px'

    this.el.el.style.height = this.notesPaneHeight + 'px'

    this.computeMainPane()
  }

  stopResize (e) {
    window.removeEventListener('mousemove', this.resize, false)
    window.removeEventListener('mouseup', this.stopResize, false)
  }

  // TODO -- rename
  // compute what of main pane?
  // and what is the main pane? the notes pane?
  computeMainPane () {
    const {containerId} = this.context.editor.props
    const mainPane = document.getElementById('content-panel-' + containerId)
    mainPane.style.height = this.el.el.offsetTop - 80 + 'px'
  }

  getProvider () {
    return this.context.notesProvider
  }

  // TODO -- do we need this?
  dispose () {
    const provider = this.getProvider()
    provider.off(this)
  }
}

export default Notes
